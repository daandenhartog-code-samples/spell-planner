# Action Planner
This code sample is taken from a hobby project of mine, a turn based grid game based on the likes of [Dofus](https://www.youtube.com/watch?v=C10eSD5QqXc). The ActionPlanner showcases how the AI creates a list of actions based on the given spells, action points and movement points. 

### Explanation
If a spell is out of range or does not have line of sight to the target the Action Planner will check if there's an available position for the entity to move to. This is done as follows:

| <img src="/Images/screen00.png"  width="200" height="200" alt="Figure #1"> | <img src="/Images/screen01.png"  width="200" height="200" alt="Figure #2"> | <img src="/Images/screen02.png"  width="200" height="200" alt="Figure #3"> |
|:---:|:---:|:---:|
| *Figure #1* | *Figure #2* | *Figure #3* |

| <img src="/Images/screen03.png"  width="200" height="200" alt="Figure #4"> | <img src="/Images/screen04.png"  width="200" height="200" alt="Figure #5"> |
|:---:|:---:|
| *Figure #4* | *Figure #5* |

Figure #1: A *Knight* and an *Orc* are on the grid. The *Knight* has movement points equal to 4, which means it can move to any of the green tiles.  
Figure #2: The *Knight* has a spell with a minimum range of 1 and a maximum range of 6, indicated by the blue tiles.  
Figure #3: The Action Planner checks which tiles the *Orc* could hit with the spell, even though it's the turn of the *Knight*.  
Figure #4: The Action Planner gets the intersecting tiles between where the *Knight* can move to and where the *Orc* could cast the spell. All cyan tiles indicate which tile the *Knight* could move to while being in the correct range of its spell.  
Figure #5: All orange tiles are cyan tiles which have line of sight to the target.

### Not included in this code sample
In the full project a [Dijkstra map](http://www.roguebasin.com/index.php?title=The_Incredible_Power_of_Dijkstra_Maps) is implemented as well. Each entity has a preferred range to its target, and by intersecting the possible tiles with the Dijkstra map it's easy to pick a tile close to the preferred range.

| <img src="/Images/screen05.png"  width="200" height="200" alt="Figure #6"> | <img src="/Images/screen06.png"  width="200" height="200" alt="Figure #7"> |
|:---:|:---:|
| *Figure #6* | *Figure #7* |

Figure #6: Displays the Dijkstra map, where the *Orc* is the target position  
Figure #7: Displays the intersection between the possible positions and the Dijkstra map, where the H value indicates the distance to the target.