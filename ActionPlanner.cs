﻿using System.Linq;
using System.Collections.Generic;

public static class ActionPlanner {

    public static List<IAction> GetActions(List<Spell> spells, Point2 positionSelf, Point2 positionTarget, int actionPoints, int movementPoints, bool[,] occupiedTiles) {
        List<Node> open = new List<Node>() { new Node(null, new Status(positionSelf, actionPoints, movementPoints), 0) };
        List<Node> closed = new List<Node>();

        while (open.Count > 0) {
            Node node = open[0];
            Status status = node.status;

            foreach (Spell spell in spells) {
                // Continue if not enough action points are available
                if (status.actionPoints - spell.GetCost() < 0)
                    continue;

                // If Spell locations contains target and line of sight is true add Spell node
                if (spell.GetPossibleLocations(status.position).Contains(positionTarget) && GridUtilities.CanDrawLine(status.position, positionTarget, occupiedTiles)) {
                    open.Add(new NodeSpell(spell, node, new Status(status.position, status.actionPoints - spell.GetCost(), status.movementPoints), spell.GetScore()));
                } else {
                    // Get intersecting tiles between tiles which are possible to move to and tiles in range of attack
                    List<Point2> possibleTilesWithinRange = MovementPlanner.GetAvailablePositions(status.position, status.movementPoints, occupiedTiles)
                        .Intersect(spell.GetPossibleLocations(positionTarget))
                        .ToList();

                    if (possibleTilesWithinRange.Count == 0)
                        continue;

                    List<Point2> path = MovementPlanner.GetPathToClosestPosition(status.position, possibleTilesWithinRange, occupiedTiles);

                    // Add node Movement to new found position in range
                    // Add node Spell with the new position and spell
                    NodeMovement nodeMovement = new NodeMovement(path, node, new Status(path[path.Count - 1], status.actionPoints, status.movementPoints - path.Count - 1), 0);
                    NodeSpell nodeSpell = new NodeSpell(spell, nodeMovement, new Status(nodeMovement.status.position, status.actionPoints - spell.GetCost(), nodeMovement.status.movementPoints), spell.GetScore());

                    open.Add(nodeSpell);
                }
            }

            open.Remove(node);
            closed.Add(node);
        }

        // Get the highest scoring node and return an list of IAction's
        return GetNodesToActions(GetNodePath(closed.OrderBy(n => n.scoreTotal).ToList()[0]));
    }

    #region Utility
    private static List<IAction> GetNodesToActions(List<Node> nodes) {
        List<IAction> actions = new List<IAction>();

        // Add IAction based on node
        foreach (Node node in nodes) {
            if (node is NodeSpell) {
                actions.Add(new CastSpell(((NodeSpell)node).spell));
            } else if (node is NodeMovement) {
                actions.Add(new MoveTo(((NodeMovement)node).path));
            }
        }

        return actions;
    }

    private static List<Node> GetNodePath(Node node) {
        List<Node> nodes = new List<Node>();

        while (node.parent != null) {
            nodes.Add(node);
            node = node.parent;
        }

        nodes.Reverse();
        return nodes;
    }
    #endregion

    #region Nodes
    private struct Status {

        public Point2 position;
        public int actionPoints;
        public int movementPoints;

        public Status(Point2 position, int actionPoints, int movementPoints) {
            this.position = position;
            this.actionPoints = actionPoints;
            this.movementPoints = movementPoints;
        }
    }

    private class Node {

        public Node parent;
        public Status status;
        public int scoreTotal { get { return score + (parent == null ? 0 : parent.scoreTotal); } }

        private int score;

        public Node(Node parent, Status status, int score) {
            this.parent = parent;
            this.status = status;
            this.score = score;
        }
    }

    private class NodeSpell : Node {

        public Spell spell;

        public NodeSpell(Spell spell, Node parent, Status status, int score)
            : base(parent, status, score) {
            this.spell = spell;
        }
    }

    private class NodeMovement : Node {

        public List<Point2> path;

        public NodeMovement(List<Point2> path, Node parent, Status status, int score)
            : base(parent, status, score) {
            this.path = path;
        }
    }
    #endregion
}